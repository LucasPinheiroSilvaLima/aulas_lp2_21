<?php

defined('BASEPATH') OR exit('No direct script access allowed');
include_once APPPATH.'libraries/util/CI_Object.php';

class ContaDataBuilder extends CI_Object {
    $contas = [
        ['parceiro' => 'Magalu',
        'descricao' => 'Notebook',
        'valor' => '2000',
        'mes' => 1,
        'ano' => 2021,
        'tipo' => 'pagar'],

        ['parceiro' => 'Casas Bahia',
        'descricao' => 'Notebook',
        'valor' => '3000',
        'mes' => 1,
        'ano' => 2021,
        'tipo' => 'pagar'],

        ['parceiro' => 'IFSP',
        'descricao' => 'Salário',
        'valor' => '6000',
        'mes' => 1,
        'ano' => 2021,
        'tipo' => 'receber'],

    ];

    public function start(){
        $this->load->library('conta');

        foreach($this->contas as $conta){
            $this->conta->cria($conta);
        }
    }

    public function clear(){
        $this->db->truncate('conta');
    }
}