<?php 

class First extends TestCase{

    public function setUp(): void{
        $this->resetInstance();
    }

    function testContaDeveRetornarRegistrosDoMesDezembro(){
        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 12, 2020);
        $this->assertEquals(4, sizeof($res));
    }


    function testContaDeveRetornarRegistroCorretamente(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->lista('pagar', 1, 2021);

        $this->assertEquals('Magalu', $res[0]['parceiro']);
        $this->assertEquals(2021, $res[0]['ano']);
        $this->assertEquals(1, $res[0]['mes']);
        $this->assertEquals(3, sizeof($res));
    } 

    function testContaDeveInformarTotalDeContasAPagarEAReceber(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->total('pagar', 1, 2021);

        $this->assertEquals(5097.25, $res);
    }

    function testContaDeveCalcularSaldoMensal(){
        $this->CI->load->library('builder/ContaDataBuilder', null, 'builder');
        $this->CI->builder->clear();
        $this->CI->builder->start();

        $this->CI->load->library('conta');
        $res = $this->CI->conta->saldo(1, 2021);

        $this->assertEquals(-875.07, $res);
    }
}